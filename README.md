# TheFork test app

The main goal of this test is to write a single screen application.

## Demo

![demo](demo.gif)

## Key concepts

- MVVC-C architecture
- Autolayout
- Use dependency injection and protocols
- For persistance the app uses UserDefaults...in real life probably we can use CoreData for local storage

## Constraints
- You should write your application in Swift
- Your code should run starting iOS 12
- Your views must be defined programmatically (no xib nor storyboard for cells)
- Restaurants data is accessible on this JSON file and must be fetched through a network call (Do not add the JSON file to your project)