//
//  thefork_devUITests.swift
//  thefork_devUITests
//
//  Created by Fabio Nisci on 22/11/21.
//

import XCTest

class thefork_devUITests: XCTestCase {

    override func setUpWithError() throws {
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {

    }

    func test_navButton_for_sorting() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        let rightNavBarButton = app.navigationBars.children(matching: .button).firstMatch
        XCTAssert(rightNavBarButton.exists)

    }
    
    func test_collectionViewsItems() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()

        let cv_cells = app.collectionViews.cells
        XCTAssert(cv_cells.count > 1)

    }
}
