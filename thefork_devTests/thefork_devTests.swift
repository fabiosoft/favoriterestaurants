//
//  thefork_devTests.swift
//  thefork_devTests
//
//  Created by Fabio Nisci on 22/11/21.
//

import XCTest
@testable import thefork_dev

class thefork_devTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_parseRestaurantViewModel(){
        let rests = MockRestaurant().restaurants()
        XCTAssert(rests.count == 1)
        
        XCTAssertEqual(rests.first!.title, "Curry Garden")
    }

}
