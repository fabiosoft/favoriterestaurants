//
//  FavoriteStore.swift
//  thefork_dev
//
//  Created by Fabio Nisci on 21/11/21.
//

import Foundation

protocol Store {
    func add(restaurant: RestaurantHighlightViewModel)
    func remove(restaurant: RestaurantHighlightViewModel)
    func status(restaurant: RestaurantHighlightViewModel) -> Bool
}

class FavoriteStore : Store {
    func add(restaurant: RestaurantHighlightViewModel){
        UserDefaults.standard.set(true, forKey: "fav \(restaurant.uuid)")
        UserDefaults.standard.synchronize()
    }
    
    func status(restaurant: RestaurantHighlightViewModel) -> Bool{
        return UserDefaults.standard.bool(forKey: "fav \(restaurant.uuid)")
    }
    
    func remove(restaurant: RestaurantHighlightViewModel){
        UserDefaults.standard.removeObject(forKey: "fav \(restaurant.uuid)")
        UserDefaults.standard.synchronize()
    }
}
