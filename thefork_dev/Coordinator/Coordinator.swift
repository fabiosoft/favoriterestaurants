//
//  Coordinator.swift
//  thefork_dev
//
//  Created by Fabio Nisci on 22/11/21.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    func start()
}

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = RestaurantsViewController(viewModel: HomeViewModel(service: NetworkClient()))
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
}
