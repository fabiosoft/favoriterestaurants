//
//  ViewController.swift
//  thefork_dev
//
//  Created by Fabio Nisci on 21/11/21.
//

import UIKit

protocol Network {
    func restaurants(_ completion: @escaping (Result<[RestaurantInfo], NetworkError>) -> Void)
    func restaurantImage(restaurant: RestaurantInfo, completion: @escaping (Result<ImageResult?, NetworkError>) -> Void) -> NetworkSessionTask?
}

enum NetworkError: Error {
    case NetworkError
    case MalformedURL
    case MalformedData
}

class ImageResult: NSObject {
    var image: UIImage?
    var metadata: String?
    init(_ image: UIImage?, metadata: String?) {
        self.image = image
        self.metadata = metadata
    }
}

protocol NetworkSession {
    func loadData(from url: URL,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> NetworkSessionTask
}
protocol NetworkSessionTask {
    func start()
    func stop()
}

extension URLSessionDataTask : NetworkSessionTask {
    func start() {
        resume()
    }
    func stop() {
        cancel()
    }
}
extension URLSession: NetworkSession {
    func loadData(from url: URL,
                  completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> NetworkSessionTask{
        return dataTask(with: url) { (data, response, error) in
            completionHandler(data, response, error)
        }
    }
}

class NetworkClient : Network {
    private let session: NetworkSession
    
    init(session: NetworkSession = URLSession.shared) {
        self.session = session
    }
    
    func restaurants(_ completion: @escaping (Result<[RestaurantInfo], NetworkError>) -> Void) {
        
        let url = URL(string: "https://alanflament.github.io/TFTest/test.json")
        guard let url = url else {
            completion(.failure(.MalformedURL))
            return;
        }
        let task = session.loadData(from: url) { data, response, error in
            guard let data = data, error == nil else {
                completion(.failure(.NetworkError))
                return
            }
            let root = try? JSONDecoder().decode(Restaurant.self, from: data)
            let restaurants = root?.data
            completion(.success(restaurants ?? []))
            
        }
        task.start()
    }
    
    func restaurantImage(restaurant: RestaurantInfo, completion: @escaping (Result<ImageResult?, NetworkError>) -> Void) -> NetworkSessionTask? {
        let uuid = restaurant.uuid
        guard let image = restaurant.mainPhoto?.the480X270 else {
            return nil
        }
        let imageURL = URL(string: image)
        guard let imageURL = imageURL else {
            return nil
        }
        return session.loadData(from: imageURL) { data, response, error in
            guard let data = data, error == nil else {
                completion(.failure(.NetworkError))
                return
            }
            if data.count <= 0{
                completion(.failure(.MalformedData))
                return
            }
            let image = UIImage(data: data)
            let result = ImageResult(image, metadata: uuid)
            completion(.success(result))
        }
    }
}
