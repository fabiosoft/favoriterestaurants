//
//  RestaurantsViewController.swift
//  thefork_dev
//
//  Created by Fabio Nisci on 21/11/21.
//

import UIKit

class RestaurantsViewController: UIViewController {

    private var collectionView:UICollectionView!
    private var homeViewModel:HomeRestaurantsViewModel!
    private let refreshControl = UIRefreshControl()
    private var dataSource : [RestaurantHighlightViewModel] = []
    weak var coordinator: MainCoordinator?

    init(viewModel:HomeRestaurantsViewModel? = HomeViewModel()){
        super.init(nibName: nil, bundle: nil)
        self.homeViewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        fetch()
        refreshControl.attributedTitle = NSAttributedString(string: NSLocalizedString("Pull to refresh", comment: ""))
        refreshControl.addTarget(self, action: #selector(self.fetch), for: .valueChanged)
        collectionView.addSubview(refreshControl)
    }
    
    @objc private func fetch(){
        refreshControl.beginRefreshing()
        homeViewModel.fetchRestaurants(page: 0) { [weak self] restaurantInfoVM in
            self?.refreshControl.endRefreshing()
            self?.dataSource = restaurantInfoVM
            self?.collectionView.reloadData()
        }
    }

    @objc private func shareAction(sender : UIBarButtonItem){
        let alert = UIAlertController(title: NSLocalizedString("Sort", comment: ""), message: nil, preferredStyle: .actionSheet)
        let byname = UIAlertAction(title: "by Name", style: .default) { action in
            
            let sortedDataSource = self.dataSource.sorted(by: {
                guard let title1 = $0.title, let title2 = $1.title else { return false }
                return title1 < title2 //ascending
            })
            self.dataSource = sortedDataSource
            self.collectionView.reloadData()
        }
        let byrating = UIAlertAction(title: "by Rating", style: .default) { action in
            let sortedDataSource = self.dataSource.sorted(by: {
                guard let rate1 = $0.rateValue, let rate2 = $1.rateValue else { return false }
                return rate1 > rate2 //descending
            })
            self.dataSource = sortedDataSource
            self.collectionView.reloadData()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { action in
            
        }
        alert.addAction(byname)
        alert.addAction(byrating)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    private func setupViews(){
        self.title = homeViewModel.title
        self.view.backgroundColor = .white
        
        let share = UIBarButtonItem(image: UIImage(named: "sort"), style: .plain, target: self, action: #selector(shareAction(sender:)))
        self.navigationItem.rightBarButtonItem = share
        
        self.collectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: collectionViewLayout())
        self.collectionView.contentInset = .zero
        self.collectionView.backgroundColor = self.view.backgroundColor
        
        self.collectionView.register(RestaurantHighlightCell.self, forCellWithReuseIdentifier: RestaurantHighlightCell.reuseIdentifier)
        
        self.collectionView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.collectionView)
        NSLayoutConstraint.activate([
            self.collectionView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.collectionView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.collectionView.topAnchor.constraint(equalTo: self.view.topAnchor),
            self.collectionView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
        ])
        
        setupCollectionViewDataSource()
    }
    
    func collectionViewLayout() -> UICollectionViewLayout {
//        if #available(iOS 13.0, *) {
//            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
//                                                  heightDimension: .fractionalHeight(1.0))
//            let item = NSCollectionLayoutItem(layoutSize: itemSize)
//            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
//                                                   heightDimension: .fractionalHeight(0.25))
//            // Here we are saying make me two columns.
//            let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2)
//            let spacing = CGFloat(10)
//            group.interItemSpacing = .fixed(spacing)
//
//            let section = NSCollectionLayoutSection(group: group)
//            section.interGroupSpacing = spacing
//            section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10)
//            let layout = UICollectionViewCompositionalLayout(section: section)
//            return layout
//        } else {
            let spacing : CGFloat = 8.0
            let columnLayout = ColumnFlowLayout(
                cellsPerRow: 2,
                minimumInteritemSpacing: spacing,
                minimumLineSpacing: spacing,
                sectionInset: UIEdgeInsets(top: spacing, left: spacing, bottom: spacing, right: spacing)
            )
            columnLayout.scrollDirection = .vertical
            return columnLayout
//        }
        
    }
    
    private func setupCollectionViewDataSource(){
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
    }
}

extension RestaurantsViewController : UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RestaurantHighlightCell.reuseIdentifier, for: indexPath) as! RestaurantHighlightCell
        cell.model = self.dataSource[indexPath.item]
        return cell
    }
    
    
}
