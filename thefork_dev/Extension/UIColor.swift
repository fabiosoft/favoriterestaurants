//
//  UIColor.swift
//  thefork_dev
//
//  Created by Fabio Nisci on 21/11/21.
//

import UIKit.UIColor

extension UIColor {
    
    // The Fork system color
    open class var appGreen: UIColor {
        return UIColor(red: 0.346, green: 0.580, blue: 0.263, alpha: 1.00)
    }
    
}
