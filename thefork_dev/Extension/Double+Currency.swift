//
//  Double+Currency.swift
//  thefork_dev
//
//  Created by Fabio Nisci on 21/11/21.
//
//

import UIKit

extension Double {

    /// Formats the receiver as a currency string using the specified three digit currencyCode. Currency codes are based on the ISO 4217 standard.
    func formatAsCurrency(currencyCode: String) -> String? {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
        currencyFormatter.currencyCode = currencyCode
        currencyFormatter.maximumFractionDigits = floor(self) == self ? 0 : 2 //is whole number?
        return currencyFormatter.string(from: NSNumber(value: self))
    }
}

