//
//  Restaurant.swift
//  thefork_dev
//
//  Created by Fabio Nisci on 21/11/21.
//

import Foundation
import UIKit

class RestaurantHighlightViewModel {
    private(set) var restaurantInfo: RestaurantInfo
    private var network : Network
    private var favoriteStore : FavoriteStore!
    
    init(_ restaurantInfo: RestaurantInfo, network : Network = NetworkClient(), store : FavoriteStore? = FavoriteStore()) {
        self.restaurantInfo = restaurantInfo
        self.network = network
        self.favoriteStore = store
    }
    
    lazy var uuid: String = {
        return self.restaurantInfo.uuid
    }()
    
    lazy var title: String? = {
        return self.restaurantInfo.name
    }()
    
    lazy var image: URL? = {
        //usually image view frame is 340x210
        if let imagePath1 = restaurantInfo.mainPhoto?.the480X270 {
            return URL(string: imagePath1)
        }
        return nil
    }()
    
    func addFavorite(){
        favoriteStore.add(restaurant: self)
    }
    
    func removeFavorite(){
        favoriteStore.remove(restaurant: self)
    }
    
    func updateFavorite(){
        if (self.isFavorite()){
            self.removeFavorite()
        }else{
            self.addFavorite()
        }
    }
    
    func isFavorite() -> Bool{
        return favoriteStore.status(restaurant: self)
    }
    
    func thumbnailImage(_ completion: @escaping (ImageResult?) -> (Void)) -> NetworkSessionTask? {
        
        return self.network.restaurantImage(restaurant: self.restaurantInfo) { result in
            switch result {
            
            case .success(let imageResult):
                completion(imageResult)
            case .failure(_):
                completion(nil)
            }
        }
    }

    lazy var speciality: String? = {
        return self.restaurantInfo.servesCuisine
    }()

    lazy var price: NSAttributedString? = {
        guard let price = self.restaurantInfo.priceRange, let currencyCode = self.restaurantInfo.currenciesAccepted else {return nil}
        let formattedAmount = Double(price).formatAsCurrency(currencyCode: currencyCode.rawValue)
        guard let amount = formattedAmount else {return nil}
        
        let output = "\(amount) \(NSLocalizedString("average price", comment: ""))"
        let outputAttributed = NSAttributedString(string: output)
        
        let priceAttributedString = NSMutableAttributedString()
        let image1Attachment = NSTextAttachment()
        image1Attachment.image = UIImage(named: "cash")
        let image1String = NSAttributedString(attachment: image1Attachment)
        priceAttributedString.append(image1String)
        priceAttributedString.append(NSAttributedString(string: " "))
        priceAttributedString.append(outputAttributed)
        return priceAttributedString
    }()

    lazy var reviews: String? = {
        guard let rateCount = self.restaurantInfo.aggregateRatings?.thefork?.reviewCount else {return nil}
        return "(\(rateCount) \(NSLocalizedString("reviews", comment: "")))"
    }()
    
    lazy var rateValue: Double? = {
        guard let rateValue = self.restaurantInfo.aggregateRatings?.thefork?.ratingValue else {return nil}
        return rateValue
    }()

    lazy var rate: NSAttributedString? = {

        guard let rateValue = self.restaurantInfo.aggregateRatings?.thefork?.ratingValue else {return nil}

        let rateValueAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.preferredFont(forTextStyle: .title2),
            .foregroundColor: UIColor.appGreen,
        ]

        let rateValueString = "\(rateValue) "
        let rateValueAttributeString = NSAttributedString(string: rateValueString, attributes: rateValueAttributes)

        if let reviews = self.reviews {

            let reviewsAttributes: [NSAttributedString.Key: Any] = [
                .font: UIFont.preferredFont(forTextStyle: .body),
                .foregroundColor: UIColor.black,
            ]
            let reviewsAttributeString = NSAttributedString(string: reviews, attributes: reviewsAttributes)

            let rateAttributedString = NSMutableAttributedString()
            let image1Attachment = NSTextAttachment()
            image1Attachment.image = UIImage(named: "tf-logo")
            let image1String = NSAttributedString(attachment: image1Attachment)
            rateAttributedString.append(image1String)
            rateAttributedString.append(NSAttributedString(string: " "))
            rateAttributedString.append(rateValueAttributeString)
            rateAttributedString.append(reviewsAttributeString)
            return rateAttributedString
        }

        return rateValueAttributeString
    }()

    lazy var promotion: NSAttributedString? = {

        guard let promotionValue = self.restaurantInfo.bestOffer?.label else {return nil}

        let promotionValueAttributes: [NSAttributedString.Key: Any] = [
            .font: UIFont.preferredFont(forTextStyle: .body),
            .foregroundColor: UIColor.white,
            .backgroundColor: UIColor.black
        ]

        let promotionValueAttributeString = NSAttributedString(string: promotionValue, attributes: promotionValueAttributes)
        return promotionValueAttributeString
    }()
}


// MARK: - Restaurant
class Restaurant: Codable {
    let data: [RestaurantInfo]?

    init(data: [RestaurantInfo]?) {
        self.data = data
    }
}

// MARK: - Datum
class RestaurantInfo: Codable {
    let uuid: String
    let name, servesCuisine: String?
    let priceRange: Int?
    let currenciesAccepted: CurrenciesAccepted?
    let address: Address?
    let aggregateRatings: AggregateRatings?
    let mainPhoto: MainPhoto?
    let bestOffer: BestOffer?

    init(name: String?, uuid: String, servesCuisine: String?, priceRange: Int?, currenciesAccepted: CurrenciesAccepted?, address: Address?, aggregateRatings: AggregateRatings?, mainPhoto: MainPhoto?, bestOffer: BestOffer?) {
        self.name = name
        self.uuid = uuid
        self.servesCuisine = servesCuisine
        self.priceRange = priceRange
        self.currenciesAccepted = currenciesAccepted
        self.address = address
        self.aggregateRatings = aggregateRatings
        self.mainPhoto = mainPhoto
        self.bestOffer = bestOffer
    }
}

class BestOffer: Codable {
    let name: String?
    let label: String?

    init(name: String?, label: String?) {
        self.name = name
        self.label = label
    }
}

// MARK: - Address
class Address: Codable {
    let street, postalCode: String?
    let locality: Locality?
    let country: Country?

    init(street: String?, postalCode: String?, locality: Locality?, country: Country?) {
        self.street = street
        self.postalCode = postalCode
        self.locality = locality
        self.country = country
    }
}

enum Country: String, Codable {
    case france = "France"
}

enum Locality: String, Codable {
    case paris = "Paris"
}

// MARK: - AggregateRatings
class AggregateRatings: Codable {
    let thefork, tripadvisor: Thefork?

    init(thefork: Thefork?, tripadvisor: Thefork?) {
        self.thefork = thefork
        self.tripadvisor = tripadvisor
    }
}

// MARK: - Thefork
class Thefork: Codable {
    let ratingValue: Double?
    let reviewCount: Int?

    init(ratingValue: Double?, reviewCount: Int?) {
        self.ratingValue = ratingValue
        self.reviewCount = reviewCount
    }
}

enum CurrenciesAccepted: String, Codable {
    case eur = "EUR"
}

// MARK: - MainPhoto
class MainPhoto: Codable {
    let source, the612X344, the480X270, the240X135: String?
    let the664X374, the1350X759, the160X120, the80X60: String?
    let the92X92, the184X184: String?

    enum CodingKeys: String, CodingKey {
        case source
        case the612X344 = "612x344"
        case the480X270 = "480x270"
        case the240X135 = "240x135"
        case the664X374 = "664x374"
        case the1350X759 = "1350x759"
        case the160X120 = "160x120"
        case the80X60 = "80x60"
        case the92X92 = "92x92"
        case the184X184 = "184x184"
    }

    init(source: String?, the612X344: String?, the480X270: String?, the240X135: String?, the664X374: String?, the1350X759: String?, the160X120: String?, the80X60: String?, the92X92: String?, the184X184: String?) {
        self.source = source
        self.the612X344 = the612X344
        self.the480X270 = the480X270
        self.the240X135 = the240X135
        self.the664X374 = the664X374
        self.the1350X759 = the1350X759
        self.the160X120 = the160X120
        self.the80X60 = the80X60
        self.the92X92 = the92X92
        self.the184X184 = the184X184
    }
}
