//
//  HomeViewModel.swift
//  thefork_dev
//
//  Created by Fabio Nisci on 21/11/21.
//

import Foundation

protocol HomeRestaurantsViewModel {
    
    var title : String { get }
    func fetchRestaurants(page: Int, completion: @escaping ([RestaurantHighlightViewModel]) -> ())
}

class HomeViewModel : HomeRestaurantsViewModel {
    private let service: Network!
    let title = "Restaurants"
    
    init(service: Network = NetworkClient()) {
        self.service = service
        
    }
    
    func fetchRestaurants(page: Int = 0, completion: @escaping ([RestaurantHighlightViewModel]) -> ()){
        service.restaurants { result in
            DispatchQueue.main.async {
                switch result {
                    
                case .success(let restaurants):
                    let viewmodels = restaurants.compactMap { RestaurantHighlightViewModel($0) }
                    completion(viewmodels)
                case .failure(_):
                    completion([])
                }
            }
        }
        
    }
}
