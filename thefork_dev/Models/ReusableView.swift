//
//  RestaurantsViewController.swift
//  thefork_dev
//
//  Created by Fabio Nisci on 21/11/21.
//

import Foundation
import UIKit

protocol Reusable : NSObject {
    static var reuseIdentifier:String {get}
    
    associatedtype DataType
    var model:DataType? {get set}
}

