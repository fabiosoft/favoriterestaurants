//
//  RestaurantsViewController.swift
//  thefork_dev
//
//  Created by Fabio Nisci on 21/11/21.
//

import UIKit

final class RestaurantHighlightCell: UICollectionViewCell, Reusable {
    
    static var reuseIdentifier: String = "RestaurantHighlightCell"
    private var thumbnailTask : NetworkSessionTask?
    
    var model: RestaurantHighlightViewModel?{
        didSet{
            self.titleLabel.text = model?.title
            self.priceLabel.attributedText = model?.price
            self.specialityLabel.text = model?.speciality
            self.reviewsLabel.attributedText = model?.rate
            self.promotionLabel.attributedText = model?.promotion
            self.favoriteButton.isSelected = model?.isFavorite() ?? false
            
            
            let placeholderLogo = UIImage(named: "tf-logo")
            self.imageView.image = placeholderLogo
            self.thumbnailTask = model?.thumbnailImage({ result in
                DispatchQueue.main.async {
                    
                    guard let result = result else {
                        self.imageView.image = placeholderLogo
                        return
                    }
                    if self.model?.uuid == result.metadata{
                        self.imageView.image = result.image
                    }
                }
            })
            self.thumbnailTask?.start()
        }
    }
//    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.thumbnailTask?.stop()
        self.imageView.image = nil
    }
//    
    lazy var titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.preferredFont(forTextStyle: .headline)
        lbl.textColor = .black
        lbl.numberOfLines = 3
        return lbl
    }()
    
    lazy var specialityLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.preferredFont(forTextStyle: .body)
        lbl.textColor = .black
        lbl.numberOfLines = 1
        return lbl
    }()
    
    @objc private func didSelectFavorite(){
        self.favoriteButton.isSelected = !self.favoriteButton.isSelected
        self.model?.updateFavorite()
    }
    
    lazy var favoriteButton: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setImage(UIImage(named: "empty-heart"), for: .normal)
        btn.setImage(UIImage(named: "filled-heart"), for: .selected)
        btn.addTarget(self, action: #selector(didSelectFavorite), for: .touchUpInside)
        btn.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        btn.layer.cornerRadius = 22
        btn.layer.masksToBounds = true
        return btn
    }()
    
    lazy var priceLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.preferredFont(forTextStyle: .body)
        lbl.textColor = .black
        lbl.numberOfLines = 1
        return lbl
    }()
    
    lazy var reviewsLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.preferredFont(forTextStyle: .body)
        lbl.textColor = .black
        lbl.numberOfLines = 1
        return lbl
    }()
    
    lazy var promotionLabel: UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.preferredFont(forTextStyle: .body)
        lbl.textColor = .black
        lbl.numberOfLines = 1
        lbl.layer.cornerRadius = 10.0
        lbl.layer.masksToBounds = true
        return lbl
    }()
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .center
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 10.0
        iv.layer.masksToBounds = true
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews(){
        contentView.backgroundColor = .white
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 4.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(self.imageView)
        NSLayoutConstraint.activate([
            self.imageView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            self.imageView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            self.imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            self.imageView.heightAnchor.constraint(equalToConstant: 100),
        ])
        
        self.contentView.addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            stackView.topAnchor.constraint(equalTo: self.imageView.bottomAnchor)
        ])
        
        stackView.addArrangedSubview(self.titleLabel)
        stackView.addArrangedSubview(self.specialityLabel)
        stackView.addArrangedSubview(self.priceLabel)
        stackView.addArrangedSubview(self.reviewsLabel)
        stackView.addArrangedSubview(self.promotionLabel)
        
        self.contentView.addSubview(self.favoriteButton)
        NSLayoutConstraint.activate([
            self.favoriteButton.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            self.favoriteButton.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor),
            self.favoriteButton.widthAnchor.constraint(equalToConstant: 44.0),
            self.favoriteButton.heightAnchor.constraint(equalToConstant: 44.0),
        ])

    }


}
